/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1937510
 */
public class PoligonalFechada extends Poligonal {
    @Override
    
    public double getComprimento() {
        double comprimento = 0.0;
        int i;
        
        for(i=0; i<getN()-1; i++){
            comprimento = comprimento + get(i).dist(get(i+1));
        }
        
        comprimento+=get(i).dist(get(0));
        
        return comprimento;
    }
}
